# Virgo educational diorama

Miniature dioramas use scale models and landscaping to create historical or fictional scenes. Such a scale model-based diorama may visualize faithfully the subject hence they became very popular in schools and museums. To take advantage of the didactic potential of dioramas we built one of them to represents the Virgo experiment. The goal of the model is the visualisation of the experiment for the audience who cannot visit the laboratory in Cascina personally. The model is useful both for educational and for outreach purposes. The project foresees preparation of simplified representation of Virgo infrastructure with interactive components for visualisation of optical system. 

## Repository content
This git repository contains LaTeX code describing how the Virgo diorama was built. The result of LaTeX compilation is stored in ```Virgo_diorama.pdf```.

```/stl-files``` - STL models for 3D printing

```Virgo_diorama.pdf``` - How to build guide-lines

## Authors and acknowledgment
The diorama is currently part of the permanent exhsibition "Dalle sfere armillari alle onde gravitazionali" hosted by the Phisics and Geology Department of the University of Perugia. It was funded by the Phisics and Geology Department of the University of Perugia and Perugia Division of the Istituto Nazionale di Fisica Nucleare (INFN).

Thanks to Helios Vocca for igniting our creativity and inspiring us to embark on the wonderful journey of building this diorama. We would like to acknowledge the substantial help of technical staff of the Phisics and Geology Department of the University of Perugia and the EGO mechanical division in the design and construction of the diorama. In particular we want thank Carlo Campeggi for the FDM printouts, Antonfanco Piluso for the preparation of plexiglass tubes and Marco Bizzarri for the preparation of electric power supply line.

## License
The code is distributed under the current GNU GPL v3 licence encouraged currently by the LVK Collaboration.